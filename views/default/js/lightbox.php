<?php
/**
 * Colorbox -- Stylesheet
 *
 * Used as a view because we need to pass a full URL to AlphaImageLoader.
 *
 * @package        Lorea
 * @subpackage     Colorbox
 *
 * Copyright 2011-2012 Lorea Faeries <federation@lorea.org>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Usage
 * Apply the class elgg-lightbox to links.
 *
 * Advanced Usage
 * Elgg is distributed with the Colorbox jQuery library. Please go to
 * http://www.jacklmoore.com/colorbox for more information on the options of this lightbox.
 *
 * Overriding
 * In a plugin, override this view and override the registration for the
 * lightbox JavaScript and CSS (@see elgg_views_boot()).
 *
 * @todo add support for passing options: $('#myplugin-lightbox').elgg.ui.lightbox(options);
 */

?>

/**
 * Lightbox initialization
 */

elgg.provide('elgg.ui.lightbox');

elgg.ui.lightbox.init = function() {

	$.extend($.colorbox.settings, {
		current: elgg.echo('js:lightbox:current', ['{current}', '{total}']),
		previous: elgg.echo('previous'),
		next: elgg.echo('next'),
		close: elgg.echo('close'),
		xhrError: elgg.echo('error:default'),
		imgError: elgg.echo('error:default'),
	});

	$(".elgg-lightbox").colorbox();
	$(".elgg-lightbox-photo").colorbox({photo: true});
	var n = 0;
	$(".elgg-lightbox-gallery").each(function() {
		$(this).find(".elgg-lightbox, .elgg-lightbox-photo")
		.addClass("elgg-lightbox-" + n)
		.colorbox({
			rel: "elgg-lightbox-" + n++,
		});
	});
}

elgg.ui.lightbox.close = function() {
	$.colorbox.close();
}

elgg.register_hook_handler('init', 'system', elgg.ui.lightbox.init);

<?php

$js_path = elgg_get_plugins_path();
$js_path = "{$js_path}colorbox/vendors/jquery/colorbox/colorbox/jquery.colorbox-min.js";
include $js_path;
